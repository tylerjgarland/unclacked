using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Unclack.TrayUI
{
    public partial class Configuration : Form
    {
        public Configuration()
        {
            InitializeComponent();
        }

        private void LoadSettings(object sender, EventArgs e)
        {
            muteEnabled.Checked = Unclack.TrayUI.Properties.Settings.Default.MuteEnabled;
            waitNumberOfSeconds.Value = Unclack.TrayUI.Properties.Settings.Default.WaitNumOfMilliSeconds;
        }

        private void SaveSettings(object sender, FormClosingEventArgs e)
        {
            // If the user clicked "Save"
            if (this.DialogResult == DialogResult.OK)
            {
                Unclack.TrayUI.Properties.Settings.Default.MuteEnabled = muteEnabled.Checked;
                Unclack.TrayUI.Properties.Settings.Default.WaitNumOfMilliSeconds = Properties.Settings.Default.WaitNumOfMilliSeconds;
                Unclack.TrayUI.Properties.Settings.Default.Save();
            }
        }
    }
}