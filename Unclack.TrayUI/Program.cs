using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Unclack.TrayUI.Services;

namespace Unclack.TrayUI
{
    static class Program
    {
        private static KeyListener _keyListener = new KeyListener();
        private static MuteService _muteService = new MuteService();
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            _keyListener.Start();
            _keyListener.Subscribe((s, e) =>
            {
                if (e.Value)
                {
                    Console.WriteLine("Muting mic");
                    _muteService.MuteMic();
                }
                else
                {
                    Console.WriteLine("UnMuting mic");
                    _muteService.UnmuteMic();
                }
            });
            // Instead of running a form, we run an ApplicationContext.
            Application.Run(new UnclackTrayUiContext());
        }
    }
}