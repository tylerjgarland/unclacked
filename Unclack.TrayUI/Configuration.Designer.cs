namespace Unclack.TrayUI
{
    partial class Configuration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.saveButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.waitNumberOfSeconds = new System.Windows.Forms.NumericUpDown();
            this.muteEnabled = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.waitNumberOfSeconds)).BeginInit();
            this.SuspendLayout();
            // 
            // saveButton
            // 
            this.saveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.saveButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.saveButton.Location = new System.Drawing.Point(215, 111);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 1;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(134, 111);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // waitNumberOfSeconds
            // 
            this.waitNumberOfSeconds.Location = new System.Drawing.Point(13, 13);
            this.waitNumberOfSeconds.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.waitNumberOfSeconds.Minimum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.waitNumberOfSeconds.Name = "waitNumberOfSeconds";
            this.waitNumberOfSeconds.Size = new System.Drawing.Size(58, 20);
            this.waitNumberOfSeconds.TabIndex = 3;
            this.waitNumberOfSeconds.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // muteEnabled
            // 
            this.muteEnabled.AutoSize = true;
            this.muteEnabled.Checked = true;
            this.muteEnabled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.muteEnabled.Location = new System.Drawing.Point(13, 40);
            this.muteEnabled.Name = "muteEnabled";
            this.muteEnabled.Size = new System.Drawing.Size(135, 17);
            this.muteEnabled.TabIndex = 4;
            this.muteEnabled.Text = "Mute mic when typing?";
            this.muteEnabled.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(80, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(210, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Number of milliseconds to wait after typing?";
            // 
            // Configuration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(314, 146);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.muteEnabled);
            this.Controls.Add(this.waitNumberOfSeconds);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.saveButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Configuration";
            this.Text = "Configuration";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SaveSettings);
            this.Shown += new System.EventHandler(this.LoadSettings);
            ((System.ComponentModel.ISupportInitialize)(this.waitNumberOfSeconds)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.NumericUpDown waitNumberOfSeconds;
        private System.Windows.Forms.CheckBox muteEnabled;
        private System.Windows.Forms.Label label1;
    }
}

