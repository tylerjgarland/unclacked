﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Unclack.TrayUI.Services
{
    public class KeyListener : IDisposable
    {
        [DllImport("user32.dll")]
        public static extern int GetAsyncKeyState(Int32 i);

        private TimeSpan _offset = TimeSpan.FromMilliseconds(Properties.Settings.Default.WaitNumOfMilliSeconds);

        private event EventHandler<KeyFiredArgs> KeysFired = (s, e) => { };

        private bool _running;
        private Task _task;
        private DateTime _lastKeyStroke = new DateTime(1990, 12, 2);
        private bool _keysFired;
        private CancellationTokenSource _cancellationToken;

        public void Start()
        {
            _running = true;
            _cancellationToken = new CancellationTokenSource();
            _task = Task.Factory.StartNew(() =>
           {
               while (_running)
               {
                   if (_cancellationToken.Token.IsCancellationRequested)
                   {
                       _running = false;
                       _cancellationToken.Token.ThrowIfCancellationRequested();
                   }

                   Thread.Sleep(100);
                   if (_keysFired && DateTime.UtcNow > _lastKeyStroke.Add(_offset))
                   {
                       _keysFired = false;
                       KeysFired(this, new KeyFiredArgs() { Value = _keysFired });
                   }
                   for (int i = 0; i < 255; i++)
                   {
                       int state = GetAsyncKeyState(i);
                       Keys k = ((Keys)i);
                       if (state != 0 && ((k >= Keys.A && k <= Keys.Z) || (k >= Keys.D0 && k <= Keys.D9)))
                       {
                           if (!_keysFired && DateTime.UtcNow > _lastKeyStroke.Add(_offset))
                           {
                               _keysFired = true;
                               KeysFired(this, new KeyFiredArgs() { Value = _keysFired });
                           }
                           _lastKeyStroke = DateTime.UtcNow;
                       }
                   }
               }
           }, _cancellationToken.Token);
        }
        public void Subscribe(Action<object, KeyFiredArgs> action) => KeysFired += (s, e) => { action(s, e); };


        public void Stop()
        {
            HaltListening();
        }

        public void Dispose()
        {
            HaltListening();
        }

        private void HaltListening()
        {
            _cancellationToken.Cancel();
            _running = false;

            _task.Dispose();

            Delegate[] clientList = KeysFired.GetInvocationList();
            foreach (var d in clientList)
                KeysFired -= (d as EventHandler<KeyFiredArgs>);
        }
    }

    public class KeyFiredArgs : EventArgs
    {
        public bool Value { get; set; }
    }
}
