﻿using NAudio.CoreAudioApi;

namespace Unclack.TrayUI.Services
{
    public class MuteService
    {
        public void SetMicMuteStatus(bool doMute)
        {
            var device = GetPrimaryMicDevice();

            if (device != null)
            {
                device.AudioEndpointVolume.Mute = doMute;
                UpdateMicStatus(device);
            }
            else
            {
                UpdateMicStatus(null);
            }
        }

        public void MuteMic()
        {
            SetMicMuteStatus(true);
        }

        public void UnmuteMic()
        {
            SetMicMuteStatus(false);
        }

        private void UpdateMicStatus(MMDevice device)
        {
            DisposeDevice(device);
        }

        private MMDevice GetPrimaryMicDevice()
        {
            var enumerator = new MMDeviceEnumerator();
            var result = enumerator.GetDefaultAudioEndpoint(DataFlow.Capture, Role.Communications);
            enumerator.Dispose();

            return result;
        }

        private void DisposeDevice(MMDevice device)
        {
            if (device != null)
            {
                device.AudioEndpointVolume.Dispose();
                device.Dispose();
            }
        }
    }
}
